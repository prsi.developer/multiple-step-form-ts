import {useState}  from 'react';
import { Link } from 'react-router-dom';

function useButton(){
    const [fromStep, setFormStep] = useState(0);
    const completeFormStep = () => {
        setFormStep((cur) => cur + 1);
      };
      const prevFormStep = () => {
        setFormStep((cur) => cur - 1);
      };
    const renderButton = () => {
        if (fromStep === 0) {
          return (
            <div>
              <button onClick={completeFormStep} type="button">
                Next Step
              </button>
            </div>
          );
        } else if (fromStep === 3) {
          return (
            <div>
              <button onClick={prevFormStep} type="button">
                Prev step
              </button>
              <button onClick={completeFormStep} type="button">
                Sign In
              </button>
            </div>
          );
        } else if (fromStep === 4) {
          return (
            <div>
              <p>Now youre a member of the cloud</p>
            </div>
          );
        } else {
          return (
            <div>
              <button onClick={prevFormStep} type="button">
                Prev step
              </button>
              <button onClick={completeFormStep} type="button">
                Next step
              </button>
            </div>
          );
        }
      };

      return {renderButton, fromStep}
}
export default useButton