import React from "react";
import "./styles.css";
import { useForm } from "react-hook-form";
import useButton from "../../hooks/useButton";

type UserData = {
  userName: string;
  email: string;
  terms_and_conditions: boolean;
};

type UserPlan = {
  duration_months: number;
  amount: number;
  up_front_payment: boolean;
};
/* type subscription_plans = {
   duration_months: number; price_usd_per_gb: number }
; */

type UserCreditCard = {
  cardNumber: number;
  expDate: number;
  cardCsv: number;
};

type User = {
  userData: UserData;
  userCreditCard: UserCreditCard;
  userPlan: UserPlan;
};

function MultiStepForm() {
  const { renderButton, fromStep } = useButton();

  const {
    watch,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = handleSubmit((data: User) => {
    console.log("data", data);
  });

  return (
    <main>
      <div className="MultiStepForm">
        <h1>
          Multiple Step Form <tr /> with Typescript
        </h1>

        <form onSubmit={onSubmit}>
          {fromStep === 0 && (
            <section>
              <div className="MultiStepForm__field">
                <label>Subscription Plan</label>
                <select
                  id="UserPlan.duration_months"
                  {...register("UserPlan.duration_months")}
                >
                  <option id="1" value={1}>
                    1
                  </option>
                  <option id="2" value={2}>
                    2
                  </option>
                  <option id="3" value={3}>
                    3
                  </option>
                </select>
                {/*  {errors.duration_months && (
                  <p>errors.duration_months.message</p>
                )} */}
              </div>
              <div className="MultiStepForm__field">
                <label htmlFor="amount">Amount</label>
                <select
                  id="UserPlan.amount"
                  {...register("UserPlan.amount")}
                  required={true}
                >
                  <option id="1" value={1}>
                    1
                  </option>
                  <option id="2" value={2}>
                    2
                  </option>
                  <option id="3" value={3}>
                    3
                  </option>
                </select>
                {/*   {errors.amount && <p>errors.amount.message</p>} */}
              </div>
              <div className="MultiStepForm__field">
                <label htmlFor="UserPlan.upFront">Up Front Payment</label>
                <p>Check this option to get a 10% discount</p>
                <input
                  type="checkBox"
                  id="UserPlan.upFront"
                  {...register("UserPlan.upFront")}
                  required={true}
                />
              </div>
            </section>
          )}
          {fromStep === 1 && (
            <section>
              <div className="MultiStepForm__field">
                <label htmlFor="UserCreditCard.creditCard">
                  Credit Card Number
                </label>
                <input
                  type="number"
                  id="UserCreditCardcreditCard"
                  placeholder="Credit Card"
                  pattern="/^5[1-5]\d{2}(| |-)(?:\d{4}\1){2}\d{4}$/"
                  {...register("UserCreditCard.creditCard")}
                  required={true}
                />
              </div>
              <div className="MultiStepForm__field">
                <label htmlFor="expDate">Exp. Date</label>
                <input
                  type="month"
                  min="2021-6"
                  max="2075-12"
                  id="UserCreditCard.expDate"
                  placeholder="Exp. Date"
                  {...register("UserCreditCard.expDate")}
                  required={true}
                />
              </div>
              <div className="MultiStepForm__field">
                <label htmlFor="cardCsv">CSV</label>
                <input
                  type="number"
                  id="UserCreditCard.cardCsv"
                  placeholder="CSV"
                  pattern="[\d]{3}"
                  {...register("UserCreditCard.cardCsv")}
                  required={true}
                />
              </div>
            </section>
          )}
          {fromStep === 2 && (
            <section>
              <div className="MultiStepForm__field">
                <label htmlFor="userName">Full Name</label>
                <input
                  type="text"
                  id="userName"
                  placeholder="Full Name"
                  {...register("userName")}
                  required={true}
                />
              </div>
              <div className="MultiStepForm__field">
                <label htmlFor="email">Email</label>
                <input
                  type="text"
                  id="email"
                  placeholder="Email"
                  {...register("email")}
                  required={true}
                />
              </div>
              <div className="MultiStepForm__field">
                <label htmlFor="terms_and_conditions">
                  Termns and Conditions
                </label>
                <input
                  type="checkBox"
                  id="terms_and_conditions"
                  pattern="^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$"
                  {...register("termns_and_conditions")}
                  required={true}
                />
              </div>
            </section>
          )}
          {fromStep === 3 && (
            <section>
              <h4>Resume</h4>
              <p>{JSON.stringify(watch(), null, 2)}</p>
            </section>
          )}
          {fromStep === 4 && (
            <section>
              <h4>Welcome</h4>
            </section>
          )}
          {renderButton()}
          <p>{JSON.stringify(watch(), null, 2)}</p>
        </form>
      </div>
    </main>
  );
}

export default MultiStepForm;
