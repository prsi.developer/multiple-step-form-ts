import React from "react";
import "./App.css";
import MultiStepForm from "./components/MultiStepForm";

function App() {
  return (
    <main className="App">
      <header className="App-header">Header</header>
      <section>
        <MultiStepForm />
      </section>
    </main>
  );
}

export default App;
